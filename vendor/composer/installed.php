<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'feb5b1a83e1c7a0cb52d4a6be55ca03064235675',
    'name' => 'microweber/microweber',
  ),
  'versions' => 
  array (
    'arcanedev/php-html' => 
    array (
      'pretty_version' => '5.0.1',
      'version' => '5.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '51e851c5029dffebcc012238056b172779428828',
    ),
    'arcanedev/seo-helper' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '84a040569b1d0f6bcd9703b21d0ed6bbfe3df22c',
    ),
    'arcanedev/support' => 
    array (
      'pretty_version' => '8.1.0',
      'version' => '8.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b161c3c080b314e832410295011625721fbd3a2f',
    ),
    'asm89/stack-cors' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '073cc8782acaeb447bde12e71f8f0b183fa8a5f5',
    ),
    'barryvdh/laravel-debugbar' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.5.x-dev',
      ),
      'reference' => 'cae0a8d1cb89b0f0522f65e60465e16d738e069b',
    ),
    'barryvdh/laravel-dompdf' => 
    array (
      'pretty_version' => 'v0.8.7',
      'version' => '0.8.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '30310e0a675462bf2aa9d448c8dcbf57fbcc517d',
    ),
    'brick/math' => 
    array (
      'pretty_version' => '0.9.2',
      'version' => '0.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dff976c2f3487d42c1db75a3b180e2b9f0e72ce0',
    ),
    'clue/stream-filter' => 
    array (
      'pretty_version' => 'v1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aeb7d8ea49c7963d3b581378955dbf5bc49aa320',
    ),
    'composer/ca-bundle' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.x-dev',
      ),
      'reference' => '9dea32b6bb602918b0144d4784b166cb95d45099',
    ),
    'composer/composer' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '472c917b2a083ec7d2fa25c55fd099d1300e2515',
    ),
    'composer/installers' => 
    array (
      'pretty_version' => 'v1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b93bcf0fa1fccb0b7d176b0967d969691cd74cca',
    ),
    'composer/package-versions-deprecated' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.x-dev',
      ),
      'reference' => 'f921205948ab93bb19f86327c793a81edb62f236',
    ),
    'composer/semver' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '647490bbcaf7fc4891c58f47b825eb99d19c377a',
    ),
    'composer/spdx-licenses' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.x-dev',
      ),
      'reference' => '8729d194e028b76ea3e84cd157bba3950203a7db',
    ),
    'composer/xdebug-handler' => 
    array (
      'pretty_version' => '1.4.5',
      'version' => '1.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f28d44c286812c714741478d968104c5e604a1d4',
    ),
    'cordoval/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'darkaonline/l5-swagger' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'b916b389312ba30009c9a05efad7ab1c911319e9',
    ),
    'davedevelopment/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'defuse/php-encryption' => 
    array (
      'pretty_version' => 'v2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f407c43b953d571421e0020ba92082ed5fb7620',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.13.x-dev',
      'version' => '1.13.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'c66f06b7c83e9a2a7523351a9d5a4b55f885e574',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.11.x-dev',
      ),
      'reference' => 'cf07cdb79d6acc6469aa2a4e88d9ae34172a67ce',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => '1.7.x-dev',
      'version' => '1.7.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '436a4eb4535f141c6c52a3099016cb22ceca05b5',
    ),
    'doctrine/common' => 
    array (
      'pretty_version' => '2.6.x-dev',
      'version' => '2.6.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '7bce00698899aa2c06fe7365c76e4d78ddb15fa3',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => 'v2.5.3',
      'version' => '2.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '2fbcea96eae34a53183377cdbb0b9bec33974648',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '1.4.x-dev',
      'version' => '1.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '319a8e5f362b0d22d487194c0349eed59ee0ba37',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.5.x-dev',
      'version' => '1.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '6410c4b8352cb64218641457cef64997e6b784fb',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.3.x-dev',
      'version' => '1.3.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '59bfb3b9be04237be4cd1afea9bbb58794c25ce8',
    ),
    'dompdf/dompdf' => 
    array (
      'pretty_version' => 'v0.8.6',
      'version' => '0.8.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db91d81866c69a42dad1d2926f61515a1e3f42c5',
    ),
    'dragonmantank/cron-expression' => 
    array (
      'pretty_version' => 'v3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a8c6e56ab3ffcc538d05e8155bb42269abf1a0c',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.1.x-dev',
      ),
      'reference' => '0dbf5d78455d4d6a41d186da50adc1122ec066f4',
    ),
    'erusev/parsedown' => 
    array (
      'pretty_version' => '1.7.4',
      'version' => '1.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb17b6477dfff935958ba01325f2e8a2bfa6dab3',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '1354e7e8c558dc9ae353e07b8cde7ab0d46e7a25',
    ),
    'facade/flare-client-php' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ef0f5bce23b30b32d98fd9bb49c6fa37b40eb546',
    ),
    'facade/ignition' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.x-dev',
      ),
      'reference' => '672d3091ccfc89d7b74e29242cd0cc442fca19ab',
    ),
    'facade/ignition-contracts' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c921a1cdba35b68a7f0ccffc6dffc1995b18267',
    ),
    'fideloper/proxy' => 
    array (
      'pretty_version' => '4.4.1',
      'version' => '4.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c073b2bd04d1c90e04dc1b787662b558dd65ade0',
    ),
    'filp/whoops' => 
    array (
      'pretty_version' => '2.10.0',
      'version' => '2.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ecda5217bf048088b891f7403b262906be5a957',
    ),
    'finlet/flexmail' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '458b08560a704e14e24165892caff3bf14993b00',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v5.2.1',
      'version' => '5.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f42c9110abe98dd6cfe9053c49bc86acc70b2d23',
    ),
    'florianv/exchanger' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.x-dev',
      ),
      'reference' => '00f7d4fa35bb0af619b63a393c1e68be5a70f9ea',
    ),
    'florianv/swap' => 
    array (
      'pretty_version' => '4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '88edd27fcb95bdc58bbbf9e4b00539a2843d97fd',
    ),
    'fruitcake/laravel-cors' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '4f92f55ba2d19874e36c8dc56a4fbdf4ba86af28',
    ),
    'fzaninotto/faker' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.9.x-dev',
      ),
      'reference' => '5ffe7db6c80f441f150fc88008d64e64af66634b',
    ),
    'geoip2/geoip2' => 
    array (
      'pretty_version' => 'v2.11.0',
      'version' => '2.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd01be5894a5c1a3381c58c9b1795cd07f96c30f7',
    ),
    'graham-campbell/markdown' => 
    array (
      'pretty_version' => '13.1.x-dev',
      'version' => '13.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'd25b873e5c5870edc4de7d980808f1a8e092a9c7',
    ),
    'graham-campbell/result-type' => 
    array (
      'pretty_version' => '1.0.x-dev',
      'version' => '1.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'cce288e91826d6d33d76b57f1ad4bdc3f3a8c1d6',
    ),
    'graham-campbell/security-core' => 
    array (
      'pretty_version' => '3.2.x-dev',
      'version' => '3.2.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'ab58f3c52538f6ec941d9238e5a75fa1ed6967d5',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '7.3.x-dev',
      ),
      'reference' => '01129f635f45659fd4764a533777d069a978bc9d',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.4.x-dev',
      ),
      'reference' => '8e7d04f1f6450fef59366c399cfad4b9383aa30d',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'a67cdbf85690e54a7b92fe91c297b20d2607c0b2',
    ),
    'halaxa/json-machine' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '88d71d3bddafe7c1772939ebc53fccf35d38f35d',
    ),
    'hamcrest/hamcrest-php' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.1.x-dev',
      ),
      'reference' => '8c3d0a3f6af734494ad8f6fbbee0ba92422859f3',
    ),
    'illuminate/auth' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/broadcasting' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/bus' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/cache' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/collections' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/config' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/console' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/container' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/contracts' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/cookie' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/database' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/encryption' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/events' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/filesystem' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/hashing' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/http' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/log' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/macroable' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/mail' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/notifications' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/pagination' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/pipeline' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/queue' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/redis' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/routing' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/session' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/support' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/testing' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/translation' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/validation' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'illuminate/view' => 
    array (
      'replaced' => 
      array (
        0 => '8.x-dev',
      ),
    ),
    'jaybizzle/crawler-detect' => 
    array (
      'pretty_version' => 'v1.2.105',
      'version' => '1.2.105.0',
      'aliases' => 
      array (
      ),
      'reference' => '719c1ed49224857800c3dc40838b6b761d046105',
    ),
    'jean85/pretty-package-versions' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.x-dev',
      ),
      'reference' => 'a917488320c20057da87f67d0d40543dd9427f7a',
    ),
    'jenssegers/agent' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.0.x-dev',
      ),
      'reference' => '1d91c71bc076a60061e0498216c0caf849eced94',
    ),
    'jeremeamia/superclosure' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.4.x-dev',
      ),
      'reference' => '5344ca13846e50196712c997f3426543d130ea31',
    ),
    'justinrainbow/json-schema' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '2ba9c8c862ecd5510ed16c6340aa9f6eadb4f31b',
    ),
    'knplabs/knp-menu' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.2.x-dev',
      ),
      'reference' => 'ef12f80738fdfb64f5b241c626986abdac2491b4',
    ),
    'kodova/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'laravel/framework' => 
    array (
      'pretty_version' => '8.x-dev',
      'version' => '8.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'a64526c55354205cf2623f85c9af48fa0664dc5a',
    ),
    'laravel/passport' => 
    array (
      'pretty_version' => 'v10.1.2',
      'version' => '10.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9f1a5d56eb609250104afc38cf407f7c2520cda3',
    ),
    'laravel/sanctum' => 
    array (
      'pretty_version' => '2.x-dev',
      'version' => '2.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '120870d168d7a045a75b3c0bd2c0d31a163cfb16',
    ),
    'laravel/socialite' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '6.x-dev',
      ),
      'reference' => '52c2cca06c84f6aeec8ed3082749724d41d9a2a4',
    ),
    'laravel/tinker' => 
    array (
      'pretty_version' => '2.x-dev',
      'version' => '2.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '9c26de963dd2adee1abbc2ca9cc6f296e4b2afbe',
    ),
    'laravelcollective/html' => 
    array (
      'pretty_version' => '6.x-dev',
      'version' => '6.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'ae15b9c4bf918ec3a78f092b8555551dd693fde3',
    ),
    'lcobucci/clock' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '13fda71158032b805cb9c8c7544e6c99019dc267',
    ),
    'lcobucci/jose-parsing' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ea0eaed79c3a5ca9ece3f824ecfe1e798fd1e1f4',
    ),
    'lcobucci/jwt' => 
    array (
      'pretty_version' => '4.0.0-alpha3',
      'version' => '4.0.0.0-alpha3',
      'aliases' => 
      array (
      ),
      'reference' => '487d0296cb89d11c023ccacc294f86802cf7bde0',
    ),
    'league/commonmark' => 
    array (
      'pretty_version' => '1.6.x-dev',
      'version' => '1.6.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'b4f0086ab4a18d60577118b062ce6a3d9f3c9c39',
    ),
    'league/csv' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9.x-dev',
      ),
      'reference' => 'b518855c7e2c8b017f338f7ceb0b0ff6cd35c947',
    ),
    'league/event' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2cc124cf9a3fab2bb4ff963307f60361ce4d119',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '1bf07fc4389ea8199d0029173e94b68731c5a332',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b9dff8aaf7323590c1d2e443db701eb1f9aa0d3',
    ),
    'league/oauth1-client' => 
    array (
      'pretty_version' => 'v1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1e7e6be2dc543bf466236fb171e5b20e1b06aee6',
    ),
    'league/oauth2-server' => 
    array (
      'pretty_version' => '8.2.4',
      'version' => '8.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '622eaa1f28eb4a2dea0cfc7e4f5280fac794e83c',
    ),
    'league/oauth2server' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'league/omnipay' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.1.x-dev',
      ),
      'reference' => 'e9439db0633ba988e6f6cdd029fad38aad73f9f6',
    ),
    'lncd/oauth2' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'maennchen/zipstream-php' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4c5803cc1f93df3d2448478ef79394a5981cc58',
    ),
    'mailerlite/mailerlite-api-v2-php-sdk' => 
    array (
      'pretty_version' => '0.3.0',
      'version' => '0.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e473fc3a5b7825c5d7a7dba441a8038f99f8183f',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '2.0.x-dev',
      'version' => '2.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '9999f1432fae467bc93c53f357105b4c31bb994c',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '361c0f545c3172ee26c3d596a0aa03f0cef65e6a',
    ),
    'maximebf/debugbar' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.16.x-dev',
      ),
      'reference' => '001da347e68505203a729a2750a5a3ecf494f8a2',
    ),
    'maxmind-db/reader' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '07f84d969cfc527ce49388558a366ad376f1f35c',
    ),
    'maxmind/web-service-common' => 
    array (
      'pretty_version' => 'v0.8.1',
      'version' => '0.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '32f274051c543fc865e5a84d3a2c703913641ea8',
    ),
    'microweber-packages/helper-functions' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/laravel-tagged-file-cache' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-backup-manager' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-cache-manager' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-captcha-manager' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-class-alias' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'c3f1671d5701a431c8895c44209f205ef57940db',
    ),
    'microweber-packages/microweber-cms' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-config' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-core' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-database-manager' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-event-manager' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-forms-manager' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-helpers' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-option-manager' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-package-manager' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-shop' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-template-manager' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-user-manager' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-packages/microweber-utils' => 
    array (
      'replaced' => 
      array (
        0 => 'dev-master',
      ),
    ),
    'microweber-templates/new-world' => 
    array (
      'pretty_version' => '1.8',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9c80c6483c220a145b826b2b3c2f5952f7a45d5a',
    ),
    'microweber/microweber' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'feb5b1a83e1c7a0cb52d4a6be55ca03064235675',
    ),
    'mobiledetect/mobiledetectlib' => 
    array (
      'pretty_version' => '2.8.37',
      'version' => '2.8.37.0',
      'aliases' => 
      array (
      ),
      'reference' => '9841e3c46f5bd0739b53aed8ac677fa712943df7',
    ),
    'mockery/mockery' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.4.x-dev',
      ),
      'reference' => 'd1339f64479af1bee0e82a0413813fe5345a54ea',
    ),
    'moneyphp/money' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.x-dev',
      ),
      'reference' => 'a44e2f2bb6bf048596af1649cb7c51dd421a979a',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.x-dev',
      ),
      'reference' => 'c1971982c3b792e488726468046b1ef977934c6a',
    ),
    'mtdowling/cron-expression' => 
    array (
      'replaced' => 
      array (
        0 => '^1.0',
      ),
    ),
    'mtrajano/laravel-swagger' => 
    array (
      'pretty_version' => 'v0.6.4',
      'version' => '0.6.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '20823090a4b910fafe4f2431bbc1f0d4cfee1251',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
      'replaced' => 
      array (
        0 => '1.x-dev',
      ),
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '46cf3d8498b095bd33727b13fd5707263af99421',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.x-dev',
      ),
      'reference' => '2fd2c4a77d58a4e95234c8a61c5df1f157a91bf4',
    ),
    'nette/finder' => 
    array (
      'pretty_version' => 'v2.5.x-dev',
      'version' => '2.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '56c89ac03c793de7774f5dc105192834480580ba',
    ),
    'nette/utils' => 
    array (
      'pretty_version' => 'v3.2.x-dev',
      'version' => '3.2.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '967cfc4f9a1acd5f1058d76715a424c53343c20c',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.10.4',
      'version' => '4.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6d052fc58cb876152f89f532b95a8d7907e7f0e',
    ),
    'nunomaduro/collision' => 
    array (
      'pretty_version' => 'v5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aca63581f380f63a492b1e3114604e411e39133a',
    ),
    'nyholm/psr7' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.4.x-dev',
      ),
      'reference' => '902515f50a970235f4930527b48d5c4e67e6c4c9',
    ),
    'ocramius/package-versions' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.99',
      ),
    ),
    'omnipay/authorizenet' => 
    array (
      'pretty_version' => '3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ccce3190c00d0a129c62dc1d1451d0125bab640',
    ),
    'omnipay/common' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.0.x-dev',
      ),
      'reference' => 'e1ebc22615f14219d31cefdf62d7036feb228b1c',
    ),
    'omnipay/mollie' => 
    array (
      'pretty_version' => 'v5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '89d6436737bc82c636b2862952dbdd53b993d359',
    ),
    'omnipay/paypal' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.0.x-dev',
      ),
      'reference' => 'dbd850839b63a1aa9d5aaae4f6d710d1cd3f0e84',
    ),
    'omnipay/stripe' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.1.x-dev',
      ),
      'reference' => 'fbf57b33c34f227695e83c1cc92d9844f88ed5e8',
    ),
    'opis/closure' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.6.x-dev',
      ),
      'reference' => '06b4961aabf900c72a20b7000bfa10fd7ae3035e',
    ),
    'orchestra/testbench' => 
    array (
      'pretty_version' => 'v6.2.0',
      'version' => '6.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2a927bae79546e6d96a4a059c83bc1428bef7f5d',
    ),
    'orchestra/testbench-core' => 
    array (
      'pretty_version' => 'v6.4.1',
      'version' => '6.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '837ff047bb023683877b34ae3714081d41eebd10',
    ),
    'paragonie/constant_time_encoding' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f34c2b11eb9d2c9318e13540a1dbc2a3afbd939c',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.99',
      'version' => '9.99.99.0',
      'aliases' => 
      array (
      ),
      'reference' => '84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '85265efd3af7ba3ca4b2a2c34dbfc5788dd29133',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bae7c545bef187884426f042434e561ab1ddb182',
    ),
    'phenx/php-font-lib' => 
    array (
      'pretty_version' => '0.5.2',
      'version' => '0.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ca6ad461f032145fff5971b5985e5af9e7fa88d8',
    ),
    'phenx/php-svg-lib' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '0.3.x-dev',
      ),
      'reference' => '16fd4d717be18d8a604f2d0c3045b35aa2f6b0a5',
    ),
    'php-http/async-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'php-http/client-common' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.3.x-dev',
      ),
      'reference' => 'e37e46c610c87519753135fb893111798c69076a',
    ),
    'php-http/client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'php-http/discovery' => 
    array (
      'pretty_version' => '1.13.0',
      'version' => '1.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '788f72d64c43dc361e7fcc7464c3d947c64984a7',
    ),
    'php-http/guzzle7-adapter' => 
    array (
      'pretty_version' => '0.1.1',
      'version' => '0.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1967de656b9679a2a6a66d0e4e16fa99bbed1ad1',
    ),
    'php-http/httplug' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.x-dev',
      ),
      'reference' => '191a0a1b41ed026b717421931f8d3bd2514ffbf9',
    ),
    'php-http/message' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fb0dbce7355cad4f4f6a225f537c34d013571f29',
    ),
    'php-http/message-factory' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '597f30e6dfd32a85fd7dbe58cb47554b5bad910e',
    ),
    'php-http/message-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'php-http/promise' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.1.x-dev',
      ),
      'reference' => '4c4c1f9b7289a2ec57cde7f1e9762a5789506f88',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.x-dev',
      ),
      'reference' => 'cf8df60735d98fd18070b7cab0019ba0831e219c',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '5.x-dev',
      ),
      'reference' => 'f8d350d8514ff60b5993dd0121c62299480c989c',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '6759f2268deb9f329812679e9dcb2d0083b2a30b',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.15.0',
      'version' => '1.15.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8e8068b31b8119e1daa5b1eb5715a3a8ea8305f',
    ),
    'phpoption/phpoption' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.7.x-dev',
      ),
      'reference' => '994ecccd8f3283ecf5ac33254543eb0ac946d525',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '624f514e889b93ba881381048ce810266c658ad2',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.13.0',
      'version' => '1.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be1996ed8adc35c3fd795488a653f4b518be70ea',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '9.2.x-dev',
      'version' => '9.2.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'ad069801f3d0cdb7102e58afd5f9f32834ec7160',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.0.x-dev',
      ),
      'reference' => 'b2ce4cf415b9989fac88e8c27c39b5ba2faad72b',
    ),
    'phpunit/php-invoker' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.1.x-dev',
      ),
      'reference' => 'e2905d5648ac5e9bd0aa85b50d240e5890f76493',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => 'e6a2483ffd3659d723996fb8b2ca638244b87e7c',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '5.0.x-dev',
      ),
      'reference' => 'c0187813193d3709a455b94916bbee2881a1c6e3',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '9.5.x-dev',
      'version' => '9.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'aad51c0706d4feaf8baead8154c7e7ed8c53931d',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.x-dev',
      'version' => '1.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => 'aa4f89e91c423b516ff226c50dc83f824011c253',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '22b2ef5687f43679481615605d7a15c557ce85b1',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '36fa03d50ff82abcae81860bdaf4ed9a1510c7cd',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => 'efd67d1dc14a7ef4fc4e518e7dee91c271d524e4',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.1.x-dev',
      ),
      'reference' => 'a18c1e692e02b84abbafe4856c3cd7cc6903908c',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '5a7b96b1dda5d957e01bc1bfe77dcca09c5a7474',
    ),
    'psy/psysh' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '0.10.x-dev',
      ),
      'reference' => '16b660d0f6be733af66aead5e6664433432a88e2',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'ramsey/collection' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '28a5c4ab2f5111db6a60b2b4ec84057e0f43b9c1',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd4032040a750077205918c86049aa0f43d22947',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '4.1.1',
      ),
    ),
    'roundcube/plugin-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'rtconner/laravel-tagging' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4bdad8266a1e47a96f1bb5e77f68d223466ac6a0',
    ),
    'sabberworm/php-css-parser' => 
    array (
      'pretty_version' => '8.3.1',
      'version' => '8.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd217848e1396ef962fb1997cf3e2421acba7f796',
    ),
    'scssphp/scssphp' => 
    array (
      'pretty_version' => 'v1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba86c963b94ec7ebd6e19d90cdab90d89667dbf7',
    ),
    'sebastian/cli-parser' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '845853b8c553f6b61d9a708b8f26066806bcc7dd',
    ),
    'sebastian/code-unit' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '1fc9f64c0927627ef78ba436c9b17d967e68e120',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => 'ab4d610891809670894a4fc260c17e5d5960ba4c',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '4.0.x-dev',
      ),
      'reference' => 'd3d66b8faa86ac57e1236d576ad003f73097c9cd',
    ),
    'sebastian/complexity' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '739b35e53379900cc9ac327b2147867b8b6efd88',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '4.0.x-dev',
      ),
      'reference' => 'e81849c6dfbe34442b4685fa457fd6f012370e54',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '5.1.x-dev',
      ),
      'reference' => 'c25633688d84a9f8694989223479051b5a8a23e7',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '4.0.x-dev',
      ),
      'reference' => '9119858d42f3963d01b737f029bb90f8464fd0ca',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '5.0.x-dev',
      ),
      'reference' => 'fe610de5530e3d29007134f76ee8dc79581a607d',
    ),
    'sebastian/lines-of-code' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1c2e997aa3146983ed888ad08b15470a2e22ecc',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '4.0.x-dev',
      ),
      'reference' => '01ab82e49081de59e2da70c351d5f698c77c33c5',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '7ed67aee59862b40785138f0203e86f1fde1b93a',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '4.0.x-dev',
      ),
      'reference' => '249976376508ed7e83b6dc429cd883a44b2a3c51',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '3.0.x-dev',
      ),
      'reference' => '0f4443cb3a1d92ce809899753bc0d5d5a8dd19a8',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.3.x-dev',
      ),
      'reference' => 'e02c851008e26557b4f1b4ffd139b71c96937b04',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c1022351a901512170118436c764e473f6de8c',
    ),
    'seld/jsonlint' => 
    array (
      'pretty_version' => '1.8.3',
      'version' => '1.8.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ad6ce79c342fbd44df10ea95511a1b24dee5b57',
    ),
    'seld/phar-utils' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8674b1d84ffb47cc59a101f5d5a3b61e87d23796',
    ),
    'shama/baton' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'simshaun/recurr' => 
    array (
      'pretty_version' => 'v4.0.2',
      'version' => '4.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd6f85ec8652366f45f6d1ba07292c9653939631a',
    ),
    'spatie/laravel-permission' => 
    array (
      'pretty_version' => '4.0.0',
      'version' => '4.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7936ea9ebbd0d91877109dfeda1659fc4570a6cc',
    ),
    'swagger-api/swagger-ui' => 
    array (
      'pretty_version' => 'v3.45.0',
      'version' => '3.45.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1ba7af074f97c872a64a415e0507c11cf8f3601b',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '6.2.x-dev',
      ),
      'reference' => '27d5ef808a066747d60f5329985949a559ec35a0',
    ),
    'symfony/config' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '2740b1695d3a9275a62f43fa01da1ac492c17930',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '36e4ff2188cb5af6e6e94560b4aaa8042933aa58',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'dfcc8276b4bea4c6cfd08b195169f0060d2a78f0',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => '4.4.x-dev',
      'version' => '4.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '157bbec4fd773bae53c5483c50951a5530a2cc16',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '8d7e2e3f396b8f5f8a86e6e62e5b2a414a52eebb',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.4.x-dev',
      ),
      'reference' => '49dc45a74cbac5fffc6417372a9f5ae1682ca0b4',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '63acc71a4a2d8eb292eec2c8d0bb7229adb49eb0',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '36b7381b24e5dd64f561073186cfffedb2d988db',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.4.x-dev',
      ),
      'reference' => '9b7cabf9fac8f2c7c7e6b2e9eeb041165a7e3327',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'b84b3a9461a96e295e3c452caae277450f89fcbe',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '0d639a0943822626290d169965804f79400e6a04',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.4.x-dev',
      ),
      'reference' => 'ad87ba5134e681ecb269424156545ded1c954249',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '7365c360e3aad973880fdb65e43f1ec4b04252e9',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '95ba3ddde73a07a28de523b4a475e6713ad42e5b',
    ),
    'symfony/intl' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'b1d0321467bd40beb1a8e10ae3842a7c0f9df2d9',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'd71b25157b329519d28b9db398c58f81541aff4a',
    ),
    'symfony/options-resolver' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '5d0f633f9bbfcf7ec642a2b5037268e61b0a62ce',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.22.x-dev',
      ),
      'reference' => 'c6c942b1ac76c82448322025e084cadc56048b4e',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.22.x-dev',
      ),
      'reference' => '06fb361659649bcfd6a208a0f1fcaf4e827ad342',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.22.x-dev',
      ),
      'reference' => '5601e09b69f26c1828b13b6bb87cb07cddba3170',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.22.x-dev',
      ),
      'reference' => '3709eb82b37c8d6eb23cf10ef19b27b3312d1632',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.22.x-dev',
      ),
      'reference' => '43a0283138253ed1d48d352ab6d0bdb3f809f248',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.22.x-dev',
      ),
      'reference' => '5232de97ee3b75b0360528dae24e73db49566ab1',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.20.x-dev',
      ),
      'reference' => '54b8cd7e6c1643d78d011f3be89f3ef1f9f4c675',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.22.x-dev',
      ),
      'reference' => 'cc6e6f9b39fe8075b3dabfbaf5b5f645ae1340c9',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.22.x-dev',
      ),
      'reference' => 'a678b42e92f86eca04b7fa4c0f6f19d097fb69e2',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.22.x-dev',
      ),
      'reference' => 'dc3063ba22c2a1fd2f45ed856374d79114998f91',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'b8d6eff26e48187fed15970799f4b605fa7242e4',
    ),
    'symfony/psr-http-message-bridge' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.1.x-dev',
      ),
      'reference' => '87fabb9e1655a1afa66ff8d12771aa2cccc079de',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '8253a8c266d998eeb245e7d04aed823ce48d1f77',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.4.x-dev',
      ),
      'reference' => '3d72b4bfab3e991aa66906aa301aa479de4ca6ee',
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/string' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '01454c66c88a6bb4449dcdeb913e463e075f331b',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.0.0',
      'version' => '5.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e86df1b0f1672362ecf96023faf2c42241c41330',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '2.4.x-dev',
      ),
      'reference' => '502490047e9064e6f1bc331ab1d43ca3da68983d',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '401993ef351ad60a95e6059c9a50f746f1a3724d',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '0887a2c153b836b521bc8f58103f24890834cef6',
    ),
    'symplify/autowire-array-parameter' => 
    array (
      'pretty_version' => '8.3.48',
      'version' => '8.3.48.0',
      'aliases' => 
      array (
      ),
      'reference' => '721e29f2e656cc3cf1101fcc0dbb4f4a44d9309b',
    ),
    'symplify/composer-json-manipulator' => 
    array (
      'pretty_version' => '8.3.48',
      'version' => '8.3.48.0',
      'aliases' => 
      array (
      ),
      'reference' => '44346fecb95fd0f435ab43d6603995d1e0796353',
    ),
    'symplify/monorepo-builder' => 
    array (
      'pretty_version' => '8.3.48',
      'version' => '8.3.48.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e945d0f541c5202eed626c576522ebab6b10a3d3',
    ),
    'symplify/package-builder' => 
    array (
      'pretty_version' => '8.3.48',
      'version' => '8.3.48.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd262d5c2043c669f145d3eabf7d8114ae64cf169',
    ),
    'symplify/set-config-resolver' => 
    array (
      'pretty_version' => '8.3.48',
      'version' => '8.3.48.0',
      'aliases' => 
      array (
      ),
      'reference' => '1333995e30b7acfa7c4378c5f85de1dd96226c93',
    ),
    'symplify/smart-file-system' => 
    array (
      'pretty_version' => '8.3.48',
      'version' => '8.3.48.0',
      'aliases' => 
      array (
      ),
      'reference' => '4146069c725b7c6e2f53924a151286f1e26c132f',
    ),
    'symplify/symplify-kernel' => 
    array (
      'pretty_version' => '8.3.48',
      'version' => '8.3.48.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c79dc6ce559b408c08e9fbc044f59cbb64961cf9',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '75a63c33a8577608444246075ea0af0d052e452a',
    ),
    'ticketswap/omnipay-przelewy24' => 
    array (
      'pretty_version' => '2.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '62121d8fcbb5103e53656fb5fe0d0d583a567a89',
    ),
    'tijsverkoyen/css-to-inline-styles' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.2.x-dev',
      ),
      'reference' => 'b43b05cf43c1b6d849478965062b6ef73e223bb5',
    ),
    'tucker-eric/eloquentfilter' => 
    array (
      'pretty_version' => '2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fe52f968d2ceb9dcdc655cb54f4aee76939de489',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => '3.x-dev',
      'version' => '3.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'be5dc6a6c4efeb1560a9aa135e7a07dfeae9d298',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '5.3.x-dev',
      ),
      'reference' => 'b3eac5c7ac896e52deab4a99068e3f4ab12d9e56',
    ),
    'voku/anti-xss' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '4.1.x-dev',
      ),
      'reference' => '614010397197f728a6055d22b80f571e881e7916',
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '1.5.6',
      'version' => '1.5.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '80953678b19901e5165c56752d087fc11526017c',
    ),
    'voku/portable-utf8' => 
    array (
      'pretty_version' => '5.4.51',
      'version' => '5.4.51.0',
      'aliases' => 
      array (
      ),
      'reference' => '578f5266725dc9880483d24ad0cfb39f8ce170f7',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.10.x-dev',
      ),
      'reference' => 'f07851c5b43e4cb502c24068620e9af6cbdd953f',
    ),
    'whitecube/lingua' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a1c8df7a1beffe1c5fb29d55432fa7f2119227f4',
    ),
    'wikimedia/composer-merge-plugin' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.5.x-dev',
      ),
      'reference' => '07122bd5d0fc76b1c2441a039bd638ea82a9d465',
    ),
    'wikimedia/less.php' => 
    array (
      'pretty_version' => 'v3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a486d78b9bd16b72f237fc6093aa56d69ce8bd13',
    ),
    'zircote/swagger-php' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d172471e56433b5c7061006b9a766f262a3edfd',
    ),
  ),
);
