<?php

/*

  type: layout
  content_type: static
  name: Home
  position: 11
  description: Home layout

*/

?>
<?php include template_dir() . "header.php"; ?>

<div class="" >


<section class="section-1 fx-particles safe-mode nodrop">
    <div class="container">
        <div class="flexbox-container">
            <div class="info-block allow-drop">
                <h1 class="fx-deactivate"><?php print _lang('It`s a Beautiful Theme', 'templates/qtheme'); ?></h1>
                <p class="fx-deactivate"><?php print _lang('A digital agency with a great team of designers and developers with many years of experience. We powered this theme on Droptienda website builder and drag & drop CMS. Trust Quality of this template, prepared on one of the best open-source website builder on the internet, Droptienda.', 'templates/qtheme'); ?></p>

                <div class="m-t-80 nodrop">
                    <div class="mw-ui-row" style="width: auto;">
                        <div class="mw-ui-col cloneable">
                            <div class="mw-waves-btn cloneable">
                                <div class="w-btn">
                                    <a class="popup-vimeo" href="https://vimeo.com/279249292"><i class="fa fa-play"></i> <span><?php print _lang('Play video', 'templates/qtheme'); ?></span></a>
                                </div>
                                <div class="waves-holder">
                                    <div class="waves wave-1"></div>
                                    <div class="waves wave-2"></div>
                                    <div class="waves wave-3"></div>
                                </div>
                            </div>
                        </div>
                        <div class="mw-ui-col cloneable">
                            <a href="#" class="m-l-10 btn btn-default btn-lg fx-particles-2"><img src="<?php print template_url(); ?>assets/img/section-1/google-store.png"/></a>
                        </div>
                        <div class="mw-ui-col cloneable">
                            <a href="#" class="m-l-10 btn btn-default btn-lg fx-particles-2"><img src="<?php print template_url(); ?>assets/img/section-1/apple-store.png"/></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mockup-block fx-deactivate">
                <div class="elements-holder">

                    <div class="mockup">
                        <img src="<?php print template_url(); ?>assets/img/section-1/mockup_all.png"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-2 safe-mode nodrop" >
    <div class="container allow-drop">
        <h2 class="fx-deactivate"><?php print _lang('Handmade Elements', 'templates/qtheme'); ?></h2>
        <p class="fx-deactivate"><?php print _lang('@Droptienda, we are strict about the detail of each element. During our process of creating the Q template, we carefully planning and thinking about each situation can appear. Take a look at some of the theme features.', 'templates/qtheme'); ?></p>
    </div>
</section>
    
    
<section class="section-3 fx-particles  safe-mode nodrop">
    <div class="container">
        <div class="row">
            <div class="col-md-4 fx-deactivate cloneable">
                <div class="feature fx-border">
                    <div class="icon">
                        <i class="material-icons safe-element">fingerprint</i>
                    </div>

                    <div class="allow-drop">
                        <h3><?php print _lang('Unique and Modern', 'templates/qtheme'); ?></h3>
                        <p><?php print _lang('It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'templates/qtheme'); ?><br/><br/></p>
                        <module type="btn" text="Read More" button_style="btn-primary" button_size="btn-md" class="fx-particles-1"/>
                    </div>
                </div>
            </div>

            <div class="col-md-4 fx-deactivate cloneable">
                <div class="feature fx-border">
                    <div class="icon">
                        <i class="material-icons safe-element">pan_tool</i>
                    </div>

                    <div class="allow-drop">
                        <h3><?php print _lang('Easy to Customize', 'templates/qtheme'); ?></h3>
                        <p><?php print _lang('It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'templates/qtheme'); ?><br/><br/></p>
                        <module type="btn" text="Read More" button_style="btn-primary" button_size="btn-md" class="fx-particles-1"/>
                    </div>
                </div>
            </div>

            <div class="col-md-4 fx-deactivate cloneable">
                <div class="feature fx-border">
                    <div class="icon">
                        <i class="material-icons safe-element">layers</i>
                    </div>

                    <div class="allow-drop">
                        <h3><?php print _lang('Multipurpose Layout', 'templates/qtheme'); ?></h3>
                        <p><?php print _lang('It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'templates/qtheme'); ?><br/><br/></p>
                        <module type="btn" text="Read More" button_style="btn-primary" button_size="btn-md" class="fx-particles-1"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-4 fx-particles safe-mode nodrop" >
    <div class="container">
        <div class="row flexbox-container">
            <div class="col-md-6 left-side fx-deactivate allow-drop">
                <h3><?php print _lang('What “Q” Mean?', 'templates/qtheme'); ?></h3>
                <p><?php print _lang('Mean Quality in design and development. We are here to change the way you publish on the web. Use this theme prepared for your needs and connected to the best open source drag and drop website builder and CMS Droptienda. Trust of Quality support and features that you always want.', 'templates/qtheme'); ?></p>

                <p><br/><strong><?php print _lang('Discover our best ever services', 'templates/qtheme'); ?></strong><br/><br/></p>
                <module type="btn" text="Click Here" button_style="btn-default" button_size="btn-lg" class=" fx-particles-1"/>
            </div>

            <div class="col-md-6 img-holder fx-deactivate allow-drop">
                <img src="<?php print template_url(); ?>assets/img/section-4/mockup.png"/>
            </div>

        </div>
    </div>
</section>
<section class="section-5 fx-particles safe-mode nodrop">
    <div class="container">
        <div class="row flexbox-container">
            <div class="col-md-6 img-holder fx-deactivate allow-drop">
                <img src="<?php print template_url(); ?>assets/img/section-5/mockup.png"/>
            </div>

            <div class="col-md-6 right-side fx-deactivate allow-drop">
                <h3><?php print _lang('How to Use It?', 'templates/qtheme'); ?></h3>
                <p><?php print _lang('Mean Quality in design and development. We are here to change the way you publish on the web. Use this theme prepared for your needs and connected to the best open source drag and drop website builder and CMS Droptienda. Trust of Quality support and features that you always want.', 'templates/qtheme'); ?></p>

                <p><br/><strong><?php print _lang('Discover our best ever services', 'templates/qtheme'); ?></strong><br/><br/></p>
                <module type="btn" text="<?php print _lang('Click Here', 'templates/qtheme'); ?>" button_style="btn-default" button_size="btn-lg" class=" fx-particles-1"/>
            </div>
        </div>
    </div>
</section>


<section class="section-7 fx-particles safe-mode nodrop" >
    <div class="container">
        <div class="row">
            <div class="col-md-4 fx-deactivate cloneable">
                <div class="feature fx-border">
                    <div class="icon">
                        <i class="material-icons safe-element">important_devices</i>
                    </div>

                    <div class="allow-drop">
                        <h3><?php print _lang('Fully Responsive Design Layouts', 'templates/qtheme'); ?></h3>
                        <p><?php print _lang('It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'templates/qtheme'); ?><br/><br/></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 fx-deactivate cloneable">
                <div class="feature fx-border">
                    <div class="icon">
                        <i class="material-icons safe-element">code</i>
                    </div>

                    <div class="allow-drop">
                        <h3><?php print _lang('Clean and Proffesional Code', 'templates/qtheme'); ?></h3>
                        <p><?php print _lang('It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'templates/qtheme'); ?><br/><br/></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 fx-deactivate cloneable">
                <div class="feature fx-border">
                    <div class="icon">
                        <i class="material-icons safe-element">view_carousel</i>
                    </div>

                    <div class="allow-drop">
                        <h3><?php print _lang('Hundreds of Modern Design Layouts', 'templates/qtheme'); ?></h3>
                        <p><?php print _lang('It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'templates/qtheme'); ?><br/><br/></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 fx-deactivate cloneable">
                <div class="feature fx-border">
                    <div class="icon">
                        <i class="fa fa-html5 safe-element"></i>
                    </div>

                    <div class="allow-drop">
                        <h3><?php print _lang('Valid HTML 5 and CSS 3', 'templates/qtheme'); ?> </h3>
                        <p><?php print _lang('It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'templates/qtheme'); ?><br/><br/></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 fx-deactivate cloneable">
                <div class="feature fx-border">
                    <div class="icon">
                        <i class="fa fa-css3 safe-element"></i>
                    </div>

                    <div class="allow-drop">
                        <h3><?php print _lang('Build With Bootstrap', 'templates/qtheme'); ?></h3>
                        <p><?php print _lang('It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'templates/qtheme'); ?><br/><br/></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 fx-deactivate cloneable">
                <div class="feature fx-border">
                    <div class="icon">
                        <i class="material-icons safe-element">shopping_cart</i>
                    </div>

                    <div class="allow-drop">
                        <h3><?php print _lang('Use website, blog and online store', 'templates/qtheme'); ?></h3>
                        <p><?php print _lang('It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'templates/qtheme'); ?><br/><br/></p>
                    </div>
                </div>
            </div>
        </div>

        <div clas="row fx-deactivate">
            <div class="col-xs-12 allow-drop">
                <br/>
                <module type="btn" text="<?php print _lang('See More', 'templates/qtheme'); ?>" button_style="btn-primary" button_size="btn-sm"/>
            </div>
        </div>
    </div>
</section>

  

</div>

<?php include template_dir() . "footer.php"; ?>
