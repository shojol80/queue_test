<?php

/*

type: layout

name: Shop products

position: 25

*/

?>

<?php
if (!$classes['padding_top']) {
    $classes['padding_top'] = 'p-t-0';
}
if (!$classes['padding_bottom']) {
    $classes['padding_bottom'] = 'p-b-50';
}

$layout_classes = ' ' . $classes['padding_top'] . ' ' . $classes['padding_bottom'] . ' ';
?>
 <div class="page-section section pt-80 pb-120 nodrop">
        <div class="container">

            <div class="row">
                <div class="col-md-9">
                        <div class="" field="content-shop" rel="page">
                        <module type="shop/products" limit="18" description-length="70"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <!-- <module type="categories" content_id="<?php print PAGE_ID; ?>" template="horizontal-list-1"/> -->

                    <?php include TEMPLATE_DIR . 'layouts' . DS . "shop_sidebar.php" ?>
                </div>
                
            </div>

        </div>
    </div>
