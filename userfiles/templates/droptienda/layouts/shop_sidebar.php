<div class="shop-sidebar-main">

    <!-- My Edit Start -->
    <div class="edit sidebar shop-sidebar-cat" rel="inherit">
        <div class="dropdown">
            <p onclick="myFunction()" class="dropbtn">
            <input type="text" placeholder="Shop Category" id="myInput" onkeyup="filterFunction()">
            </p>
            <div id="myDropdown" class="dropdown-content">
              <module type="categories" content-id="<?php print PAGE_ID; ?>"/>
            </div>
        </div>
    </div>
</div>



<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
// window.onclick = function(event) {
//   if (!event.target.matches('.dropbtn')) {
//     var dropdowns = document.getElementsByClassName("dropdown-content");
//     var i;
//     for (i = 0; i < dropdowns.length; i++) {
//       var openDropdown = dropdowns[i];
//       if (openDropdown.classList.contains('show')) {
//         openDropdown.classList.remove('show');
//       }
//     }
//   }
// }

function filterFunction() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("myDropdown");
  a = div.getElementsByTagName("a");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
</script>




