<?php

/*
	@Category CRUD api
*/

use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Microweber\App\Providers\Illuminate\Support\Facades\Hash;
use QueryPath\Exception;
use Symfony\Component\HttpFoundation\Response;

/** @noinspection SpellCheckingInspection */
api_expose('all_categorie');
api_expose('insert_category');
api_expose('categories');
api_expose('edit_category');
api_expose('category_delete');
api_expose('get_wishlist_sessions');
api_expose('delete_wishlist_sessions');
api_expose('guest_checkout');
api_expose('set_wishlist_sessions');
api_expose('edit_wishlist_sessions');
api_expose('add_wishlist_sessions');
api_expose('remove_wishlist_sessions');
api_expose('store_update_products');
api_expose('filter_wishlist');
api_expose('share_wishlist');
api_expose('sdk_text');
api_expose('custom_api', function () {
    app()->log_manager->save('');
});
//api_expose('update_product_drm');

event_bind('mw.user.after_register', function () {
    /** @noinspection PhpUndefinedVariableInspection */
    $id = $data['id'];
    /** @noinspection SpellCheckingInspection */
    $ch4 = curl_init(config('global.drm_base_url') . 'api/dorptinda-new-customer-notify?type=customer&id=' . $id);
    // Returns the data/output as a string instead of raw data
    curl_setopt($ch4, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch4, CURLOPT_CUSTOMREQUEST, "GET");
    curl_exec($ch4);
    // get info about the request
    curl_getinfo($ch4);
    // close curl resource to free up system resources
    curl_close($ch4);
    // dd($data);
    // die();
});

api_expose('all_categories_dt');
/** @noinspection PhpUndefinedMethodInspection */
function all_categories_dt()
{
    $all_categories_dt = DB::table('categories');
    if (isset($_GET['category_id']))
        $all_categories_dt = $all_categories_dt->where('id', $_GET['category_id'])->get();
    else
        $all_categories_dt = $all_categories_dt->get();

    $all_categories_dt = json_decode($all_categories_dt);

    DB::beginTransaction();
    try {
        $client = new Client();
        foreach ($all_categories_dt as $category) {
            try {
                $client->request('POST', 'http://165.227.134.199/api/v1/catalogs/categories', [
                    'headers' => [
                        'content-type' => 'application/json',
                        'Authorization' => 'Bearer ' . config('global.token_dp'),
                    ],
                    'query' => [
                        'category_name_de' => $category->title,
                        'country_id' => 1
                    ],
                ]);
            } catch (\Exception $e) {
                dump($e->getMessage());
            }
        }
        DB::commit(); // all good
    } catch (\Exception $e) {
        DB::rollback(); // something went wrong
        $e->getMessage();
    }
}


api_expose('token_save');
function token_save($data){
    dd($data);
}


function sdk_text(){
    $setting_client_id		= 'droptiendapost';	// #### insert your own shared secret 'client id' (which you received from IT-Recht Kanzlei)
    $setting_client_secret	= 'DVRq2AgoYQ9a8WomNKuuFFRqhBFe';	// #### insert your own shared secret 'client secret' (which you received from IT-Recht Kanzlei)

    // $setting_download_PDF	= true;	// #### set true or false whether or not you want to download the legal text PDF provided. The PDF can be used for later attaching it to order confirmation emails (recommendation in Q1/2019 for legal reasons: attach 'agb' and 'widerruf', please consult a lawyer regarding the current requirements).



    $LTI = new ITRechtKanzlei\LegalTextInterface();

    $selected_multishop_id = $LTI->is_MULTISHOP(array('12345' => 'Subshop 1', '23456' => 'Subshop 2', '34567' => 'Subshop 3'));


    // read token from legal text transmission
    $token = $LTI->get_AUTH_Token();

    $token_number= DB::table('tokenurl')->first();
    if($token_number->token == $token){

        $retval_token_ok = true;

    }else{

        $retval_token_ok = true;

    }
    // report OK if shop was found / token is valid, or report that the lookup had no valid result
    if( $retval_token_ok === true ){ $LTI->send_AUTH_ok(); } else { $LTI->send_AUTH_failed(); }


    // read more values from current legal text (LT) transmission
    // #### use these values as required by your endpoint application or shop/CMS system
    $LT_Language	= $LTI->get_LT_Language();	// ISO 639-1 (lowercase, e.g. 'de' for German / Deutsch)
    $LT_Country		= $LTI->get_LT_Country();	// ISO 3166-1-alpha-2 (uppercase, e.g. 'DE' for Germany/Deutschland)
    $LT_Title		= $LTI->get_LT_Title();		// can be used e.g. as a title for a CMS page
    $LT_Text		= $LTI->get_LT_Text();		// the legal text in text form
    $LT_HTML		= $LTI->get_LT_HTML();		// the legal text in HTML form
    $LT_Type		= $LTI->get_LT_Type();		// holds the legal text type ('agb','','widerruf','impressum')



    // $data=$LT_Text;

    $pathToFile = 'mylogname.log';

    // Log the data to your file using file_put_contents;
    // file_put_contents($pathToFile, $data, FILE_APPEND);

    $retval_savetodatabase_ok=false;
    if($LT_Type=='agb'){

        $agb = DB::table('legals')->where('term_name','agb')->get()->toArray();
        // return (!empty($agb));
        if(!empty($agb)){
             DB::table('legals')->where('term_name','agb')->update(['description' => $LT_Text]);
             $retval_savetodatabase_ok=true;
        }else{
             DB::table('legals')->insert(['term_name' => 'agb','description' => $LT_Text]);
             $retval_savetodatabase_ok=true;
        }

    }else if($LT_Type=='widerruf'){

        $Widerruf = DB::table('legals')->where('term_name','widerruf')->get()->toArray();
        if(!empty($Widerruf)){
             DB::table('legals')->where('term_name','widerruf')->update(['description' => $LT_Text]);
             $retval_savetodatabase_ok=true;
        }else{
             DB::table('legals')->insert(['term_name' => 'widerruf','description' => $LT_Text]);
             $retval_savetodatabase_ok=true;
        }

    }else if($LT_Type=='impressum'){

        $impressum = DB::table('legals')->where('term_name','imprint')->get()->toArray();
        if(!empty($impressum)){
             DB::table('legals')->where('term_name','imprint')->update(['description' => $LT_Text]);
             $retval_savetodatabase_ok=true;
        }else{
             DB::table('legals')->insert(['term_name' => 'imprint','description' => $LT_Text]);
             $retval_savetodatabase_ok=true;
        }

    }else if($LT_Type=='datenschutz'){

        $datenschutz = DB::table('legals')->where('term_name','pp')->get()->toArray();
        if(!empty($datenschutz)){
             DB::table('legals')->where('term_name','pp')->update(['description' => $LT_Text]);
             $retval_savetodatabase_ok=true;
        }else{
             DB::table('legals')->insert(['term_name' => 'pp','description' => $LT_Text]);
             $retval_savetodatabase_ok=true;
        }

    }

    if( $retval_savetodatabase_ok === true ){ $LTI->send_SUCCESS(); } else { $LTI->send_ERROR('Fehler beim Speichern des Rechtstextes'); }



    // $data=$LT_Text;


        // return $_REQUEST;


    }

api_expose('update_categories_dt');
function update_categories_dt($params)
{
    dd($params);

}

/** @noinspection PhpUndefinedMethodInspection */
function categories()
{
    $all_categories = DB::table('categories');
    if (isset($_GET['category_id'])) {
        $all_categories = $all_categories->where('id', $_GET['category_id'])->get();
    } else {
        $all_categories = $all_categories->get();
    }
    return $all_categories;
}


// function insert_category()
// {
//     $data_save = array();
//     if (isset($_POST['title'])) {
//         $data_save['title'] = $_POST['title'];
//         $category_id = save_category($data_save);
//     } else {
//         $data_save['title'] = 'Category title is required';
//         return json_encode($data_save);
//     }
// }

/** @noinspection PhpUndefinedMethodInspection */
function insert_category()
{
    $new_cat = array();
    // $old_cat = $update_data->where('title',$_REQUEST['title'])->get();
    if (isset($_REQUEST['title'])) {
        $unique_check = DB::table('categories')->where('title', $_REQUEST['title'])->get()->first();
        if ($unique_check) {
            return response()->json(["message" => "title is already exists."], 422);
        } else {
            $currentTime = Carbon\Carbon::now();
            $new_cat['title'] = $_REQUEST['title'];
            $new_cat['created_by'] = 1;
            $new_cat['edited_by'] = 1;
            $new_cat['url'] = slugify($_REQUEST["title"]);
            $new_cat['parent_id'] = 0;
            $new_cat['rel_type'] = 'content';
            $new_cat['rel_id'] = 8;
            $new_cat['position'] = 0;
            $new_cat['category_subtype'] = 'default';
            $new_cat['users_can_create_content'] = 0;
            $new_cat['data_type'] = 'category';
            $new_cat["updated_at"] = $currentTime->toDateTimeString();
            $new_cat["created_at"] = $currentTime->toDateTimeString();

            DB::table("categories")->insertGetId($new_cat);
            return response()->json(["message" => "Category added successfully"], 200);
        }
    } else {
        return response()->json(["message" => "title is required"], 404);
    }
}

/** @noinspection PhpUndefinedMethodInspection */
function category_delete()
{
    if (isset($_POST['category_id'])) {
        DB::table('categories')->where('id', $_POST['category_id'])->delete();
        return response()->json(["message" => "deleted"], 204);
    } else {
        return response()->json(["message" => "category_id ie required"], 422);
    }
}

/** @noinspection PhpUndefinedMethodInspection */
function edit_category()
{
    $currentTime = Carbon\Carbon::now();
    // $old_cat = $update_data->where('title',$_REQUEST['title'])->get();
    if (isset($_REQUEST['old_title']) && isset($_REQUEST['new_title'])) {
        $unique_check = DB::table('categories')->where('title', $_REQUEST['new_title'])->get()->first();
        if ($unique_check) {
            return response()->json(["message" => "New title is already exists."], 422);
        }
        $old_cat = DB::table('categories')->where('title', $_REQUEST['old_title'])->get()->first();
        if ($old_cat) {
            DB::table('categories')->where('id', $old_cat->id)->update([
                "title" => $_REQUEST['new_title'], "url" => slugify($_REQUEST["new_title"]),
                "updated_at" => $currentTime->toDateTimeString()
            ]);
            return response()->json(["message" => "Update successful"], 200);
        } else {
            return response()->json(["message" => "Old category not found"], 404);
        }
    } else {
        $update_data = 'old_title and new_title ie required';
        return json_encode($update_data);
    }
}


/** @noinspection PhpUndefinedMethodInspection */
function get_wishlist_sessions()
{
    $is_logged = is_logged();

    if ($is_logged == true) {
        $user_id = user_id();

        $wishLists = DB::table("wishlist_sessions")->where("user_id", $user_id)->get();

        if (count($wishLists) === 0) {
            DB::table('wishlist_sessions')->insert(array('user_id' => $user_id, "name" => "Default"));
            $wishLists = DB::table("wishlist_sessions")->where("user_id", $user_id)->get();
        } else{
            foreach ($wishLists as $wishList)
                $wishList->products = DB::table('wishlist_session_products')->select('product_id')->where("user_id", $user_id)->where("wishlist_id", $wishList->id)->get();

        return response()->json($wishLists, 200);
        }
    }
    return false;
}


/** @noinspection PhpUndefinedMethodInspection */
function edit_wishlist_sessions($data)
{
    $is_logged = is_logged();

    if (isset($data['title']) && strlen($data['title']) > 0 && $is_logged == true) {
        $user_id = user_id();
        DB::table('wishlist_sessions')
                ->where('user_id', $user_id)
                ->where('name' , $data['titlehide'])
                ->update(['name' => $data['title']]);
        // DB::table('wishlist_sessions')->insert(array('user_id' => $user_id, "name" => $data['title']));
        return DB::table('wishlist_sessions')->where('user_id', $user_id)->get();
    }
    return false;
}

/** @noinspection PhpUndefinedMethodInspection */
function delete_wishlist_sessions($data)
{

    // dd($data);
    $is_logged = is_logged();

    if (isset($data['name']) && strlen($data['name']) > 0 && $is_logged == true) {
        $user_id = user_id();

        $id=DB::table('wishlist_sessions')
                ->where('user_id', $user_id)
                ->where('name' , $data['name'])
                ->first()->id;

            DB::table('wishlist_session_products')
                ->where('wishlist_id','=',$id)
                ->delete();


        DB::table('wishlist_sessions')
                ->where('user_id', $user_id)
                ->where('name' , $data['name'])
                ->delete();

        // DB::table('wishlist_sessions')->insert(array('user_id' => $user_id, "name" => $data['title']));
        return DB::table('wishlist_sessions')->where('user_id', $user_id)->get();
    }
    return false;
}

function guest_checkout($dataa)
{

    $product_ids = $dataa['iid'];
    $user_id = user_id();
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 24; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    $ins_array = array(
        'products_id' => $product_ids,
        'user_id' => $user_id,
        'slug' => $randomString,
    );



    DB::table('quick_checkout')->insert($ins_array);

//    if (url_segment(0) == 'shop') {
        $url = site_url() . 'checkout?slug=' . $randomString;
//    } else {

//        $url = site_url() .$dataa['lang']. '/checkout?slug=' . $randomString;
//    }



    return [
        'success' => true,
        'url' => $url
    ];
}

/** @noinspection PhpUndefinedMethodInspection */
function set_wishlist_sessions($data)
{
    $is_logged = is_logged();

    if (isset($data['title']) && strlen($data['title']) > 0 && $is_logged == true) {
        $user_id = user_id();
        DB::table('wishlist_sessions')->insert(array('user_id' => $user_id, "name" => $data['title']));
        return DB::table('wishlist_sessions')->where('user_id', $user_id)->get();
    }
    return false;
}

/** @noinspection PhpUndefinedMethodInspection */
function add_wishlist_sessions($data)
{
//    dd($data);
    $is_logged = is_logged();
    if ($is_logged == true) {
        $productId = $data['productId'];
        $sessionId = $data['sessionId'];
        $user_id = user_id();

        try {
            DB::table("wishlist_session_products")->updateOrInsert(["user_id" => $user_id, "product_id" => $productId, "wishlist_id" => $sessionId]);
            $wishLists = DB::table("wishlist_sessions")->where("user_id", $user_id)->get();
            foreach ($wishLists as $wishList)
                $wishList->products = DB::table('wishlist_session_products')->select('product_id')->where("user_id", $user_id)->where("wishlist_id", $wishList->id)->get();
        } catch (\Exception $exception) {
            $wishLists = DB::table("wishlist_sessions")->where("user_id", $user_id)->get();
            foreach ($wishLists as $wishList)
                $wishList->products = DB::table('wishlist_session_products')->select('product_id')->where("user_id", $user_id)->where("wishlist_id", $wishList->id)->get();
        }

        return response()->json($wishLists, 200);
    }

    return response()->json([], 401);
}

function share_wishlist($data){
    $product_ids = implode(',',$data['products']);
    $user_id = $data['user_id'];

    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 24; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    $ins_array=array(
        'products_id'=>$product_ids,
        'user_id'=>$user_id,
        'slug'=>$randomString,
    );

    DB::table('wishlist_link')->insert($ins_array);


    return [
       'success' => true,
       'url' => site_url().'shop?slug='.$randomString
    ];
}

/** @noinspection PhpUndefinedMethodInspection */
function remove_wishlist_sessions($data)
{
//    dd($data);
if($data){
    $is_logged = is_logged();
    if ($is_logged == true) {
        $productId = $data['productId'];
        $sessionId = $data['sessionId'];
        $user_id = user_id();

        if($sessionId == 'all'){
        DB::table("wishlist_session_products")->where("user_id", $user_id)->where("product_id", $productId)->delete();

        }

        DB::table("wishlist_session_products")->where("user_id", $user_id)->where("product_id", $productId)->where("wishlist_id", $sessionId)->delete();

        $wishLists = DB::table("wishlist_sessions")->where("user_id", $user_id)->get();
        foreach ($wishLists as $wishList)
            $wishList->products = DB::table('wishlist_session_products')->select('product_id')->where("user_id", $user_id)->where("wishlist_id", $wishList->id)->get();

        return response()->json($wishLists, 200);
    }

    return response()->json([], 401);
}
}

/** @noinspection PhpUndefinedMethodInspection
 * @noinspection PhpUnusedParameterInspection
 * @param $request
 * @return false|string|Response
 */
function store_update_products($request)
{
    $responseData = array();
    $headers = apache_request_headers();
    $user = null;
    foreach ($headers as $header => $value) {
        if ($header == "Authorization") {
            $token = explode(" ", $value);
            $originalToken = explode(":", base64_decode($token[1]));
            $username = $originalToken[0];
            $password = $originalToken[1];

            $user = DB::table("users")->where("email", $username)->first();

            if ($user == null || !Hash::check($password, $user->password)) return response(array(["message" => "Invalid user"]), 401);
        }
    }

    $request = json_decode(file_get_contents("php://input"), true);

    $responseData["request"] = $request;

    if ($user !== null) {
        $message = array();

        if (!isset($request["title"]) || strlen(trim($request["title"])) < 1) $message["title"] = "title is required";
        if (!isset($request["qty"]) || strlen(trim($request["qty"])) < 1) $message["qty"] = "qty is required";
        else if (!strlen(trim($request["qty"])) > 0 || !is_numeric(trim($request["qty"]))) $message["qty"] = "qty is numeric";
        if (!isset($request["ean"]) || strlen(trim($request["ean"])) < 1) $message["ean"] = "ean is required";
        else if (!strlen(trim($request["ean"])) > 0 || !is_numeric(trim($request["ean"]))) $message["ean"] = "ean is numeric";
        if (!isset($request["is_free_shipping"]) || strlen(trim($request["is_free_shipping"])) < 1) $message["is_free_shipping"] = "is_free_shipping is required";
        else if (!strlen(trim($request["is_free_shipping"])) > 0 || !(trim($request["is_free_shipping"]) === "y" || trim($request["is_free_shipping"]) === "n")) $message["is_free_shipping"] = "is_free_shipping must be enum (\"y\", \"n\")";
        if (isset($request["additional_shipping_cost"]) && (!strlen(trim($request["additional_shipping_cost"])) > 0 || !is_numeric(trim($request["additional_shipping_cost"])))) $message["additional_shipping_cost"] = "additional_shipping_cost is numeric";
        if (isset($request["shipping_depth"]) && (!strlen(trim($request["shipping_depth"])) > 0 || !is_numeric(trim($request["shipping_depth"])))) $message["shipping_depth"] = "shipping_depth is numeric";
        if (isset($request["shipping_weight"]) && (!strlen(trim($request["shipping_weight"])) > 0 || !is_numeric(trim($request["shipping_weight"])))) $message["shipping_weight"] = "shipping_weight is numeric";
        if (isset($request["shipping_height"]) && (!strlen(trim($request["shipping_height"])) > 0 || !is_numeric(trim($request["shipping_height"])))) $message["shipping_height"] = "shipping_height is numeric";
        if (isset($request["shipping_width"]) && (!strlen(trim($request["shipping_width"])) > 0 || !is_numeric(trim($request["shipping_width"])))) $message["shipping_width"] = "shipping_width is numeric";
        if (isset($request["max_qty_per_order"]) && (!strlen(trim($request["max_qty_per_order"])) > 0 || !is_numeric(trim($request["max_qty_per_order"])))) $message["max_qty_per_order"] = "max_qty_per_order is numeric";
        if (isset($request["sku"]) && (!strlen(trim($request["sku"])) > 0 || !is_numeric(trim($request["sku"])))) $message["sku"] = "sku is numeric";

        if (count($message) > 0) return response()->json($message, 422);

        $product = DB::table("content")->where("created_by", $user->id)->where("ean", $request["ean"])->first();

        $responseData["product"] = $product;

        if ($product == null) {
            $shopId = DB::table("content")->select("id")->where("is_shop", 1)->first()->id;
            $maxPosition = DB::table("content")->where("content_type", "product")->where("subtype", "product")->max("position");

            $currentTime = Carbon\Carbon::now();

            try {
                DB::table("content")->insert(["content_type" => "product", "subtype" => "product", "url" => slugify($request["title"]), "title" => $request["title"], "parent" => $shopId ?? 8, "description" => $request["description"] ?? "", "position" => $maxPosition ? intval($maxPosition) + 1 : 0, "content" => $request["title"], "content_body" => $request["description"] ?? "", "content_meta_title" => $request["content_meta_title"] ?? "", "updated_at" => $currentTime->toDateTimeString(), "created_at" => $currentTime->toDateTimeString(), "created_by" => $user->id, "edited_by" => $user->id, "posted_at" => $currentTime->toDateTimeString(), "ean" => intval($request["ean"])]);
                $product = DB::table("content")->where("created_by", $user->id)->where("ean", $request["ean"])->get()->first();
                $responseData["product"] = $product;
            } catch (Exception $exception) {
                $responseData["product"] = $exception->getMessage();
            }
        } else {
            $currentTime = Carbon\Carbon::now();
            DB::table("content")->where('id', $product->id)->update(["url" => slugify($request["title"]), "title" => $request["title"], "parent" => $shopId ?? 8, "description" => $request["description"] ?? "", "content" => $request["title"], "content_body" => $request["description"] ?? "", "content_meta_title" => $request["content_meta_title"] ?? "", "updated_at" => $currentTime->toDateTimeString(), "edited_by" => $user->id,]);
            $product = DB::table("content")->where("created_by", $user->id)->where("ean", $request["ean"])->get()->first();
            $responseData["product"] = $product;
        }

        /*
//        if (isset($request["categories"])) {
//            $categories = $request["categories"];
//            if (count($categories) > 0) {
//                DB::table("categories_items")->where("rel_id", $responseData['product']->id)->delete();
//                $shopId = DB::table("content")->select("id")->where("is_shop", 1)->first()->id;
//                foreach ($categories as $category) {
//                    $existingCategory = DB::table("categories")->where("rel_id", $shopId)->where("data_type", "category")->where("title", $category)->get()->first();
//                    if ($existingCategory == null) {
////                        save_category(array("id" => "0", "rel" => "content", "rel_id" => $shopId, "data_type" => "category", "parent_id" => "0", "title" => $category, "category" => "parent-selector: ${shopId}", "description" => $category, "users_can_create_content" => "0", "category_subtype" => "default"));
//                        $currentTime = Carbon\Carbon::now();
//                        DB::table("categories")->insert(["updated_at" => $currentTime->toDateTimeString(), "created_at" => $currentTime->toDateTimeString(), "created_by" => $user->id, "edited_by" => $user->id, "data_type" => "category", "title" => $category, "url" => slugify($category), "parent_id" => 0, "rel_type" => "content", "rel_id" => $shopId, "position" => 0, "users_can_create_content" => 0, "category_subtype" => "default"]);
//                        $existingCategory = DB::table("categories")->where("rel_id", $shopId)->where("data_type", "category")->where("title", $category)->get()->first();
//                        DB::table("categories_items")->insert(["parent_id" => $existingCategory->id, "rel_type" => "content", "rel_id" => $responseData['product']->id]);
//                    } else {
//                        DB::table("categories_items")->insert(["parent_id" => $existingCategory->id, "rel_type" => "content", "rel_id" => $responseData['product']->id]);
//                    }
//                }
//                $responseData["categories"] = DB::table("categories_items")->where("rel_id", $responseData['product']->id)->get();
//            }
//        }
        */

        if (isset($request["categories"])) {
            $requestedCategories = $request["categories"];
            DB::table("categories_items")->where("rel_id", $responseData['product']->id)->delete();
            $shopId = DB::table("content")->select("id")->where("is_shop", 1)->first()->id;
            foreach ($requestedCategories as $requestedCategory) {
                if (strpos($requestedCategory, '>') !== false) {
                    $hiddenArray = explode('>', $requestedCategory);

                    $categoryId = createOrUpdateCategory($user->id, trim($hiddenArray[0]), $shopId);
                    createOrUpdateCategoryItem($categoryId, $responseData['product']->id);
                    $categoryId = createOrUpdateCategory($user->id, trim($hiddenArray[1]), $categoryId);
                    createOrUpdateCategoryItem($categoryId, $responseData['product']->id);
                } else if (strpos($requestedCategory, ' - ') !== false) {
                    $hiddenArray = explode(' - ', $requestedCategory);

                    $categoryId = createOrUpdateCategory($user->id, trim($hiddenArray[0]), $shopId);
                    createOrUpdateCategoryItem($categoryId, $responseData['product']->id);
                    $categoryId = createOrUpdateCategory($user->id, trim($hiddenArray[1]), $categoryId);
                    createOrUpdateCategoryItem($categoryId, $responseData['product']->id);
                } else {
                    $categoryId = createOrUpdateCategory($user->id, trim($requestedCategory), $shopId);
                    createOrUpdateCategoryItem($categoryId, $responseData['product']->id);
                }
            }

            $responseData["categories"] = DB::table("categories_items")->where("rel_type", "content")->where("rel_id", $responseData['product']->id)->get();
        }


        if (isset($request["filename"])) {
            $files = explode(",", $request["filename"]);
            DB::table("media")->where("created_by", $user->id)->where("rel_type", "content")->where("rel_id", intval($responseData["product"]->id))->where("media_type", "picture")->delete();
            foreach ($files as $file) {
                DB::table("media")->insert(["updated_at" => $currentTime->toDateTimeString(), "created_at" => $currentTime->toDateTimeString(), "edited_by" => $user->id, "created_by" => $user->id, "rel_type" => "content", "rel_id" => intval($responseData["product"]->id), "media_type" => "picture", "position" => 9999999, "filename" => $file,]);
            }
            $responseData["media"] = DB::table("media")->where("created_by", $user->id)->where("rel_type", "content")->where("rel_id", intval($responseData["product"]->id))->where("media_type", "picture")->get();
        }

        if (isset($request["qty"])) {
            $prev = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "qty")->get()->first();
            if ($prev == null) {
                DB::table("content_data")->insert(["content_id" => intval($responseData["product"]->id), "field_name" => "qty", "field_value" => $request["qty"], "created_by" => $user->id, "edited_by" => $user->id, "rel_type" => "content", "rel_id" => intval($responseData["product"]->id)]);
            } else {
                DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "qty")->update(["field_value" => $request["qty"], "edited_by" => $user->id]);
            }
            $responseData["qty"] = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "qty")->get()->first();
        }
        if (isset($request["sku"])) {
            $prev = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "sku")->get()->first();
            if ($prev == null) {
                DB::table("content_data")->insert(["content_id" => intval($responseData["product"]->id), "field_name" => "sku", "field_value" => $request["sku"], "created_by" => $user->id, "edited_by" => $user->id, "rel_type" => "content", "rel_id" => intval($responseData["product"]->id)]);
            } else {
                DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "sku")->update(["field_value" => $request["sku"], "edited_by" => $user->id]);
            }
            $responseData["sku"] = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "sku")->get()->first();
        }
        if (isset($request["shipping_weight"])) {
            $prev = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_weight")->get()->first();
            if ($prev == null) {
                DB::table("content_data")->insert(["content_id" => intval($responseData["product"]->id), "field_name" => "shipping_weight", "field_value" => $request["shipping_weight"], "created_by" => $user->id, "edited_by" => $user->id, "rel_type" => "content", "rel_id" => intval($responseData["product"]->id)]);
            } else {
                DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_weight")->update(["field_value" => $request["shipping_weight"], "edited_by" => $user->id]);
            }
            $responseData["shipping_weight"] = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_weight")->get()->first();
        }
        if (isset($request["shipping_width"])) {
            $prev = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_width")->get()->first();
            if ($prev == null) {
                DB::table("content_data")->insert(["content_id" => intval($responseData["product"]->id), "field_name" => "shipping_width", "field_value" => $request["shipping_width"], "created_by" => $user->id, "edited_by" => $user->id, "rel_type" => "content", "rel_id" => intval(intval($responseData["product"]->id))]);
            } else {
                DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_width")->update(["field_value" => $request["shipping_width"], "edited_by" => $user->id]);
            }
            $responseData["shipping_width"] = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_width")->get()->first();
        }
        if (isset($request["shipping_height"])) {
            $prev = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_height")->get()->first();
            if ($prev == null) {
                DB::table("content_data")->insert(["content_id" => intval($responseData["product"]->id), "field_name" => "shipping_height", "field_value" => $request["shipping_height"], "created_by" => $user->id, "edited_by" => $user->id, "rel_type" => "content", "rel_id" => intval($responseData["product"]->id)]);
            } else {
                DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_height")->update(["field_value" => $request["shipping_height"], "edited_by" => $user->id]);
            }
            $responseData["shipping_height"] = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_height")->get()->first();
        }
        if (isset($request["shipping_depth"])) {
            DB::table("content_data")->updateOrInsert(["content_id" => intval($responseData["product"]->id), "field_name" => "shipping_depth"], ["content_id" => intval($responseData["product"]->id), "field_name" => "shipping_depth", "field_value" => $request["shipping_depth"], "created_by" => $user->id, "edited_by" => $user->id, "rel_type" => "content", "rel_id" => intval($responseData["product"]->id)]);
//            $prev = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_depth")->get()->first();
//            if ($prev == null) {
//                DB::table("content_data")->insert(["content_id" => intval($responseData["product"]->id), "field_name" => "shipping_depth", "field_value" => $request["shipping_depth"], "created_by" => $user->id, "edited_by" => $user->id, "rel_type" => "content", "rel_id" => intval($responseData["product"]->id)]);
//            } else {
//                DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_depth")->update(["field_value" => $request["shipping_depth"], "edited_by" => $user->id]);
//            }
            $responseData["shipping_depth"] = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "shipping_depth")->get()->first();
        }
        if (isset($request["is_free_shipping"])) {
            $prev = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "is_free_shipping")->get()->first();
            if ($prev == null) {
                DB::table("content_data")->insert(["content_id" => intval($responseData["product"]->id), "field_name" => "is_free_shipping", "field_value" => $request["is_free_shipping"], "created_by" => $user->id, "edited_by" => $user->id, "rel_type" => "content", "rel_id" => intval($responseData["product"]->id)]);
            } else {
                DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "is_free_shipping")->update(["field_value" => $request["is_free_shipping"], "edited_by" => $user->id]);
            }
            $responseData["is_free_shipping"] = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "is_free_shipping")->get()->first();
        }
        if (isset($request["additional_shipping_cost"])) {
            $prev = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "additional_shipping_cost")->get()->first();
            if ($prev == null) {
                DB::table("content_data")->insert(["content_id" => intval($responseData["product"]->id), "field_name" => "additional_shipping_cost", "field_value" => $request["additional_shipping_cost"], "created_by" => $user->id, "edited_by" => $user->id, "rel_type" => "content", "rel_id" => intval($responseData["product"]->id)]);
            } else {
                DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "additional_shipping_cost")->update(["field_value" => $request["additional_shipping_cost"], "edited_by" => $user->id]);
            }
            $responseData["additional_shipping_cost"] = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "additional_shipping_cost")->get()->first();
        }
        if (isset($request["max_qty_per_order"])) {
            $prev = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "max_qty_per_order")->get()->first();
            if ($prev == null) {
                DB::table("content_data")->insert(["content_id" => intval($responseData["product"]->id), "field_name" => "max_qty_per_order", "field_value" => $request["max_qty_per_order"], "created_by" => $user->id, "edited_by" => $user->id, "rel_type" => "content", "rel_id" => intval($responseData["product"]->id)]);
            } else {
                DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "max_qty_per_order")->update(["field_value" => $request["max_qty_per_order"], "edited_by" => $user->id]);
            }
            $responseData["max_qty_per_order"] = DB::table("content_data")->where("content_id", intval($responseData["product"]->id))->where("field_name", "max_qty_per_order")->get()->first();
        }

        if (isset($request["price"])) {
            $existingCustomFields = DB::table("custom_fields")->where("rel_type", "content")->where("rel_id", intval($responseData["product"]->id))->where("type", "price")->where("name", "price")->where("name_key", "price")->get()->first();
            if ($existingCustomFields == null) {
                DB::table("custom_fields")->insert(["rel_type" => "content", "rel_id" => intval($responseData["product"]->id), "type" => "price", "name" => "price", "name_key" => "price", "created_by" => $user->id, "edited_by" => $user->id, "is_active" => 1]);
            }
            $existingCustomFields = DB::table("custom_fields")->where("rel_type", "content")->where("rel_id", intval($responseData["product"]->id))->where("type", "price")->where("name", "price")->where("name_key", "price")->get()->first();
            $existingCustomFieldsValues = DB::table("custom_fields_values")->where("custom_field_id", $existingCustomFields->id)->get()->first();
            if ($existingCustomFieldsValues == null) {
                DB::table("custom_fields_values")->insert(["custom_field_id" => $existingCustomFields->id, "value" => $request['price'], "position" => 0]);
            } else {
                DB::table("custom_fields_values")->where("custom_field_id", $existingCustomFields->id)->update(["value" => $request['price']]);
            }
            $responseData["price"] = DB::table("custom_fields_values")->where("custom_field_id", $existingCustomFields->id)->get()->first();
        }

        if (isset($request["tags"])) {
            $requestTags = explode(",", $request["tags"]);
            $allPrevTags = DB::table("tagging_tagged")->where("taggable_type", "Content")->where("taggable_id", intval($responseData["product"]->id))->get();
            if (count($allPrevTags) > 0) {
                foreach ($allPrevTags as $allPrevTag) {
                    $suggestibleTag = DB::table('tagging_tags')->where('name', $allPrevTag->tag_name)->get()->first();
                    if ($suggestibleTag != null) {
                        DB::table('tagging_tags')->where('name', $allPrevTag->tag_name)->update(["count" => intval($suggestibleTag->count) - 1]);
                    }
                }
                DB::table("tagging_tagged")->where("taggable_type", "Content")->where("taggable_id", intval($responseData["product"]->id))->delete();
            }

            if (count($requestTags) > 0) {
                foreach ($requestTags as $requestTag) {
                    if ($requestTag[0] == '#') $requestTag = substr($requestTag, 1, strlen($requestTag) - 1);
                    $suggestibleTag = DB::table('tagging_tags')->where('name', $requestTag)->get()->first();
                    if ($suggestibleTag != null) {
                        DB::table('tagging_tags')->where('name', $requestTag)->update(["count" => intval($suggestibleTag->count) + 1]);
                    } else {
                        DB::table('tagging_tags')->insert(["slug" => slugify($requestTag), "name" => $requestTag, "description" => null, "suggest" => 0, "count" => 1]);
                    }
                    DB::table("tagging_tagged")->insert(["taggable_id" => intval($responseData["product"]->id), "taggable_type" => "Content", "tag_name" => $requestTag, "tag_slug" => slugify($requestTag), "tag_description" => null]);
                }
            }

            $responseData["tags"] = DB::table("tagging_tagged")->where("taggable_type", "Content")->where("taggable_id", intval($responseData["product"]->id))->get();
        }

        app()->log_manager->save('');

        return response()->json($responseData);
    } else {
        return response(array(["message" => "Invalid user"]), 401);
    }
}

/**
 * @param $userId integer
 * @param $title string
 * @param $relId integer
 * @return integer
 * @noinspection PhpUndefinedMethodInspection
 */
function createOrUpdateCategory(int $userId, string $title, int $relId)
{
    $existingCategory = DB::table("categories")->where("rel_id", $relId)->where("data_type", "category")->where("title", $title)->get()->first();
    if ($existingCategory) {
        return $existingCategory->id;
    } else {
        $currentTime = Carbon\Carbon::now();
        return DB::table("categories")->insertGetId(["created_by" => $userId, "edited_by" => $userId, "title" => $title, "url" => slugify($title), "rel_id" => $relId, "parent_id" => 0, "rel_type" => "content", "position" => 0, "users_can_create_content" => 0, "category_subtype" => "default", "updated_at" => $currentTime->toDateTimeString(), "created_at" => $currentTime->toDateTimeString(), "data_type" => "category"]);
    }
}

/**
 * @param $categoryId integer
 * @param $productId integer
 * @return void
 * @noinspection PhpUndefinedMethodInspection
 */
function createOrUpdateCategoryItem(int $categoryId, int $productId)
{
    $existingCategoryItem = DB::table("categories_items")->where("parent_id", $categoryId)->where("rel_type", "content")->where("rel_id", $productId)->get()->first();
    if (!$existingCategoryItem) {
        return DB::table("categories_items")->insert(["parent_id" => $categoryId, "rel_type" => "content", "rel_id" => $productId]);
    }
    return;
}

/** @noinspection SpellCheckingInspection */
function slugify($text)
{
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) return 'n-a';
    return $text;
}

/**
 * @param $params
 *
 * @return void
 * @noinspection PhpUndefinedMethodInspection
 */
function update_product_drm($params)
{
    $productId = $params['id'];
    $conditionalUpdate = array();

    $content = DB::table("content")->where("id", intval($productId))->get()->first();

    $conditionalUpdate["title"] = $content->title;
    $conditionalUpdate["description"] = $content->content_body;
    $conditionalUpdate["status"] = $content->is_active;

    $images = DB::table("media")->select("filename")->where("rel_id", $productId)->get();
    if (count($images) > 0) {
        $imagesArray = array();
        foreach ($images as $item) {
            $site_url = site_url();
            $array = [str_replace("{SITE_URL}", $site_url, $item->filename)];
            array_push($imagesArray, $array[0]);
        }
        $conditionalUpdate["image"] = $imagesArray;
    }

    $priceField = DB::table("custom_fields")->where("rel_id", $productId)->where("type", "price")->select('id')->get()->first();
    if ($priceField->id > 0) {
        $priceFieldValue = DB::table("custom_fields_values")->where("custom_field_id", $priceField->id)->select("value")->get()->first();
        $conditionalUpdate["vk_price"] = $priceFieldValue->value;
    }

    $tagged = DB::table('tagging_tagged')->where("taggable_id", $productId)->get();
    if (count($tagged) > 0) {
        $tags = array();
        foreach ($tagged as $item) {
            $array = [$item->tag_name];
            array_push($tags, $array[0]);
        }
        $conditionalUpdate["tags"] = implode(",", $tags);
    }

    $contentData = DB::table("content_data")->where("content_id", $productId)->get();

    if (count($contentData) > 0) {
        $size = array();
        foreach ($contentData as $contentDatum) {
            if ($contentDatum->field_name == 'qty') $conditionalUpdate["stock"] = $contentDatum->field_value;
            if ($contentDatum->field_name == 'sku') $conditionalUpdate["sku"] = $contentDatum->field_value;
            if ($contentDatum->field_name == 'shipping_weight') $conditionalUpdate["item_weight"] = $contentDatum->field_value;
            if ($contentDatum->field_name == 'shipping_height') array_push($size, $contentDatum->field_value);
            if ($contentDatum->field_name == 'shipping_width') array_push($size, $contentDatum->field_value);
            if ($contentDatum->field_name == 'shipping_depth') array_push($size, $contentDatum->field_value);
            if ($contentDatum->field_name == 'label-color') $conditionalUpdate["item_color"] = $contentDatum->field_value;
        }

        $conditionalUpdate["item_size"] = implode("x", $size);
    }

    if (count($conditionalUpdate) > 0 && $content->ean) {
        $client = new Client();
        $client->request("PUT", "http://165.227.134.199/api/v1/catalogs/products/" . $content->ean . "/update?country=1", [
            "headers" => [
                "content-type" => "application/json",
                "Authorization" => "Bearer " . config("global.token_dp"),
            ],
            "json" => $conditionalUpdate,
        ]);
    }
}

function filter_wishlist(){
    dd($_REQUEST);
}
api_expose("config_set_drm");
function config_set_drm($data){

    Config::set('microweber.userToken', $data['userToken']);
    Config::set('microweber.userPassToken', $data['userPassToken']);
    Config::save(array('microweber'));
}
